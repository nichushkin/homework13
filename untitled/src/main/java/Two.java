import java.io.*;
import java.util.Arrays;

/*

Заданы три целочисленных массива. Записать эти массивы в файл в паралельних потоках. Создать класс SaveAsThread для
представления потока, который записывает целочисленный массив в файл.
 */
public class Two {
    public static void main(String[] args) throws IOException {
        int[] array1 = new int[]{1, 2, 3, 4, 5, 6};
        int[] array2 = new int[]{7, 8, 9, 10, 11, 12, 13, 14};
        int[] array3 = new int[]{15, 16, 17, 18, 190};
        String path = "Thirteen/Files";
        File folder = new File(path);
        folder.mkdirs();
        File file = new File(path, "exit.txt");
        file.createNewFile();
        Thread thread1 = new Thread(new SaveAsThread(array1, file));
        Thread thread2 = new Thread(new SaveAsThread(array2, file));
        Thread thread3 = new Thread(new SaveAsThread(array3, file));
        thread1.start();
        thread2.start();
        thread3.start();

    }

    public static class SaveAsThread implements Runnable {
        File file;
        int[] array;

        public SaveAsThread(int[] array, File file) {
            this.array = array;
            this.file = file;
        }

        @Override
        public synchronized void run() {
            try (OutputStream outputStream = new FileOutputStream(file, true)) {
                outputStream.write(Arrays.toString(array).getBytes());
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
