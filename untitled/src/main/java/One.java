import java.util.Scanner;

/*
Пользователь вводит с клавиатуры значение в массив. После чего запускаются два потока. Первый поток находит
максимум в массиве, второй — минимум. Результаты вычислений возвращаются в метод main().

 */
public class One {
    public static void main(String[] args) throws InterruptedException {
        int size = (int) (Math.random() * 10);
//        System.out.println(size);
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[size];
        System.out.println("Необходимо ввести числа " + size + " раз");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        scanner.close();
        Thread threadOne = new Thread(new CalculateMaxValue(array));
        threadOne.start();
        Thread threadTwo = new Thread(new CalculateMinValue(array));
        threadTwo.start();
    }

    public static class CalculateMaxValue implements Runnable {
        private int[] array;

        public CalculateMaxValue(int[] array) {
            this.array = array;
        }

        @Override
        public void run() {
            int temp = array[0];
            for (int i = 1; i < array.length; i++) {
                if (temp < array[i]) {
                    temp = array[i];
                }
            }
            System.out.println("max value = " + temp);
        }
    }

    public static class CalculateMinValue implements Runnable {
        private int[] array;

        public CalculateMinValue(int[] array) {
            this.array = array;
        }

        @Override
        public void run() {
            int temp = array[0];
            for (int i = 1; i < array.length; i++) {
                if (temp > array[i]) {
                    temp = array[i];
                }
            }
            System.out.println("min value = " + temp);
        }
    }
}

