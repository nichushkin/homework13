import java.util.Scanner;

public class OneAnotherWay {

    public static void main(String[] args) throws InterruptedException {
        int size = (int) (Math.random() * 10);
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[size];
        System.out.println("Необходимо ввести числа " + size + " раз");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        Thread threadMaxValue = new Thread(new Runnable() {
            @Override
            public void run() {
                int temp = array[0];
                for (int i = 1; i < array.length; i++) {
                    if (temp < array[i]) {
                        temp = array[i];
                    }
                }
                System.out.println("max value = " + temp);
            }
        });

        Thread threadMinValue = new Thread(new Runnable() {
            @Override
            public void run() {
                int temp = array[0];
                for (int i = 1; i < array.length; i++) {
                    if (temp > array[i]) {
                        temp = array[i];
                    }
                }
                System.out.println("min value = " + temp);
            }
        });

        threadMaxValue.start();
        threadMinValue.start();

    }
}